# ConcurrentLatch
> ConcurrentLatch是一个基于JDK的多线程归类并发处理闩工具

## ConcurrentLatch使用场景

> 当你有5个无相关性操作，顺序执行那么消耗时间合将达到5个操作的所有操作时间和，如果开启多线程，那么又不能保证这5个操作都进行完毕才能进行后续操作，那么基于刚才说的场景ConcurrentLatch就是用来解决这个问题的，考虑到系统不能无限制的增加线程，所以ConcurrentLatch又增加了线程池管理器的概念，防止系统因为线程开启过多而宕机。

## git地址

> https://gitee.com/zxporz/ConcurrentLatch.git

> https://gitee.com/zxporz/ConcurrentLatch/blob/master/README.md

## 更新说明

### 2017-11-25 [1.1-SNAPSHOT]
> 本次优化修复了无返回参数还必须写一个返回类的问题

> 新增了一种代理模式，代替在返回类中定死任务名称的实现方式（不在依赖注解），可以将同一个的业务组件以不同任务来运行（感谢刘仁豪同学提的建议以及思路）

> 调整部分API，但不影响原有API的调用。新增了ConcurrentLatch工厂，默认获取代理方式的ConcurrentLatch实例



## 使用ConcurrentLatch

> ConcurrentLatch不依赖任何三方jar包

> 编写你需要并发处理的业务类以及返回对象

```
/**添加这个类运行的任务名，注意这个名字在一次线程池运行中不可重复*/
/**也可以不配置注解，写一个名为TASKNAME的字符串成员变量也可行*/
@LatchTaskName("rule")
/**需实现LatchThread接口，并将业务代码写入handle方法，否则会抛出异常*/
public class RuleLatch implements  LatchThread {
    RuleDto dto = null;
    /**通过构造方法传递入参*/
    public RuleLatch(RuleDto args){
        dto = args;
    }
    /**你的业务处理*/
    public RuleDto handle() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        dto.setMmmm(dto.getMmmm()+ 9999);
        return dto;
    }
}
/**添加这个类运行的任务名，注意这个名字在一次线程池运行中不可重复*/
/**也可以不配置注解，写一个名为TASKNAME的字符串成员变量也可行*/
/**这个配置的注解值必须和对应的LatchThread一致，否则会抛出异常*/
@LatchTaskName("rule")
public class RuleDto {
……
}
```
> 编写调用（最新方式）


```
/**创建一个ConcurrentLatch执行器*/
ConcurrentLatch excutor = ConcurrentLatchExcutorFactory.getConcurrentLatch();
/**也可以通过其他静态方法获取其他的ConcurrentLatch*/
//ConcurrentLatch excutornormal = ConcurrentLatchExcutorFactory.getConcurrentLatch(ConcurrentLatchExcutorFactory.ExcutorType.NORMAL);
//ConcurrentLatch excutorproxy = ConcurrentLatchExcutorFactory.getConcurrentLatch(ConcurrentLatchExcutorFactory.ExcutorType.PROXY);
/**组织需要并发的业(任)务(务)类对象*/
LatchThread platformLatchThread = new PlatformLatch();//业务类A
RuleDto ruleDto = new RuleDto();
ruleDto.setRuleID("zxp123");
ruleDto.setMmmm(0.00001);
LatchThread ruleLatchThread = new RuleLatch(ruleDto);//业务类B
/**将任务推入ConcurrentLatch执行器*/
excutor.put(platformLatchThread,"AAA");
excutor.put(ruleLatchThread,"BBB");
/**可以把一个之前已经有的业务组件再放入池中，只需要名称不重复即可，当然你可以可以重新new一个*/
excutor.put(platformLatchThread,"CCC");
/**通知ConcurrentLatch执行器开始执行所有推入的任务*/
/**这里会阻塞，直到所有任务都执行完毕*/
Map<String, Object> map = excutor.excute();
/**获取返回值*/
/**这里的key就是上面在注解(LatchTaskName)或者TASKNAME配置的值，能轻松的获取所有的返回对象*/
for (String key : map.keySet()) {
    Object out = map.get(key);
}
```
> 执行结果


```
我是BBB
我是AAA
我是AAA
taskname==========aaa
PlatformDto{name='0516', premium=6500.98, policyNo='000000000001'}
taskname==========ccc
PlatformDto{name='0516', premium=6500.98, policyNo='000000000001'}
taskname==========bbb
RuleDto{ruleID='zxp123', mmmm=9999.00001}
```


## 配置说明

> 可根据服务器的性能情况设置线程峰值（详见org.zxp.ConcurrentLatch.Constants）

```
/**单个线程池线程最大容量*/
CURRENT_MAX_POOL_COUNT = 4;
/**线程池最大数量*/
int MAX_EXCUTOR_COUNT = 3;
```

## 关键逻辑说明

> ConcurrentLatchExcutor中调用了线程池管理器来获取线程池

> 通过Future获取线程执行返回对象

> LatchExcutorBlockingQueueManager线程池管理器中通过阻塞队列来监控线程池的使用情况

> 代理方式put任务时，内部会吧任务重新生成代理对象（JDK）[2017-11-25]