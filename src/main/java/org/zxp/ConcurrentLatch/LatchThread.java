package org.zxp.ConcurrentLatch;

public interface LatchThread {
    public Object handle();
}
