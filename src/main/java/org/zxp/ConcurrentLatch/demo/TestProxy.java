package org.zxp.ConcurrentLatch.demo;

import org.zxp.ConcurrentLatch.ConcurrentLatch;
import org.zxp.ConcurrentLatch.ConcurrentLatchExcutor;
import org.zxp.ConcurrentLatch.ConcurrentLatchExcutorFactory;
import org.zxp.ConcurrentLatch.LatchThread;
import org.zxp.ConcurrentLatch.demo.dto.RuleDto;
import org.zxp.ConcurrentLatch.demo.service.PlatformLatch;
import org.zxp.ConcurrentLatch.demo.service.RuleLatch;

import java.util.Map;

public class TestProxy {

    public static void main(String[] args) throws Exception {
        ConcurrentLatch excutor = ConcurrentLatchExcutorFactory.getConcurrentLatch();
        LatchThread platformLatchThread = new PlatformLatch();
        LatchThread platformLatchThread2 = new PlatformLatch();
        RuleDto ruleDto = new RuleDto();
        ruleDto.setRuleID("zxp123");
        ruleDto.setMmmm(0.00001);
        LatchThread ruleLatchThread = new RuleLatch(ruleDto);

        excutor.put(platformLatchThread,"aaa");
        excutor.put(ruleLatchThread,"bbb");
        excutor.put(platformLatchThread,"ccc");
        Map<String, Object> map = excutor.excute();
        for (String key : map.keySet()) {
            System.out.println("taskname==========" + key);
            Object out = map.get(key);
            System.out.println(out);
        }
        System.out.println("看看能不能阻塞");
    }
}
